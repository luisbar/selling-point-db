-- AlterTable
ALTER TABLE `Product` MODIFY `withStock` BOOLEAN NOT NULL DEFAULT false,
    MODIFY `image` VARCHAR(191) NULL;
