/*
  Warnings:

  - Added the required column `quantity` to the `SaleDetail` table without a default value. This is not possible if the table is not empty.
  - Added the required column `subtotal` to the `SaleDetail` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `SaleDetail` ADD COLUMN `quantity` INTEGER NOT NULL,
    ADD COLUMN `subtotal` DOUBLE NOT NULL;
