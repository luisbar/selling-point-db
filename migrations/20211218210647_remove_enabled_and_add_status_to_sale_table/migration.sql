/*
  Warnings:

  - You are about to drop the column `enabled` on the `Sale` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Sale` DROP COLUMN `enabled`,
    ADD COLUMN `status` ENUM('pending', 'completed', 'canceled') NOT NULL DEFAULT 'pending';
