datasource mysql {
  provider = "mysql"
  url      = env("DATABASE_URL")
}

generator client {
  provider = "prisma-client-js"
}

enum Statuses {
  pending
  completed
  canceled
}

model User {
  id          Int      @id @default(autoincrement())
  firstName   String
  lastName    String
  username    String
  password    String
  enabled     Boolean  @default(true)
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt
  // Relations
  createdBy   User?    @relation("createdByUser", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?    @relation("updatedByUser", fields: [updatedById], references: [id])
  updatedById Int?
  scope       Scope?   @relation("scope", fields: [scopeId], references: [id])
  scopeId     Int?

  createdByUser       User[]       @relation("createdByUser")
  updatedByUser       User[]       @relation("updatedByUser")
  createdByScope      Scope[]      @relation("createdByScope")
  updatedByScope      Scope[]      @relation("updatedByScope")
  createdByCustomer   Customer[]   @relation("createdByCustomer")
  updatedByCustomer   Customer[]   @relation("updatedByCustomer")
  createdBySale       Sale[]       @relation("createdBySale")
  updatedBySale       Sale[]       @relation("updatedBySale")
  createdByInvoice    Invoice[]    @relation("createdByInvoice")
  updatedByInvoice    Invoice[]    @relation("updatedByInvoice")
  createdByProduct    Product[]    @relation("createdByProduct")
  updatedByProduct    Product[]    @relation("updatedByProduct")
  createdBySaleDetail SaleDetail[] @relation("createdBySaleDetail")
  updatedBySaleDetail SaleDetail[] @relation("updatedBySaleDetail")
  createdByCategory   Category[]   @relation("createdByCategory")
  updatedByCategory   Category[]   @relation("updatedByCategory")

  @@unique([username])
}

model Scope {
  id          Int      @id @default(autoincrement())
  name        String
  rules       Json
  enabled     Boolean  @default(true)
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt
  // Relations
  createdBy   User?    @relation("createdByScope", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?    @relation("updatedByScope", fields: [updatedById], references: [id])
  updatedById Int?

  user User[] @relation("scope")

  @@unique([name])
}

model Customer {
  id          Int      @id @default(autoincrement())
  firstName   String
  lastName    String
  email       String
  nit         String
  enabled     Boolean  @default(true)
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt
  // Relations
  createdBy   User?    @relation("createdByCustomer", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?    @relation("updatedByCustomer", fields: [updatedById], references: [id])
  updatedById Int?

  sale Sale[] @relation("sale")

  @@unique([firstName, lastName])
}

model Sale {
  id          Int       @id @default(autoincrement())
  withInvoice Boolean   @default(false)
  total       Float
  status      Statuses  @default(pending)
  createdAt   DateTime  @default(now())
  updatedAt   DateTime  @updatedAt
  // Relations
  createdBy   User?     @relation("createdBySale", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?     @relation("updatedBySale", fields: [updatedById], references: [id])
  updatedById Int?
  customer    Customer? @relation("sale", fields: [customerId], references: [id])
  customerId  Int?
  invoice     Invoice?  @relation("invoice", fields: [invoiceId], references: [id])
  invoiceId   Int?      @unique

  saleDetail SaleDetail[] @relation("saleDetailSale")
}

model Invoice {
  id                  Int      @id @default(autoincrement())
  sfc                 String
  nit                 String
  authorizationNumber String
  invoiceNumber       String
  total               Float
  emittedDate         DateTime @default(now())
  limitDate           DateTime
  controlCode         String
  customerNit         String
  customerName        String
  canceled            Boolean
  createdAt           DateTime @default(now())
  updatedAt           DateTime @updatedAt
  // Relations
  createdBy           User?    @relation("createdByInvoice", fields: [createdById], references: [id])
  createdById         Int?
  updatedBy           User?    @relation("updatedByInvoice", fields: [updatedById], references: [id])
  updatedById         Int?

  sale Sale? @relation("invoice")
}

model Product {
  id          Int       @id @default(autoincrement())
  code        Int?
  name        String
  description String
  cost        Float
  price       Float
  withStock   Boolean   @default(false)
  image       String?
  enabled     Boolean   @default(true)
  createdAt   DateTime  @default(now())
  updatedAt   DateTime  @updatedAt
  // Relations
  createdBy   User?     @relation("createdByProduct", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?     @relation("updatedByProduct", fields: [updatedById], references: [id])
  updatedById Int?
  category    Category? @relation("category", fields: [categoryId], references: [id])
  categoryId  Int?

  saleDetail SaleDetail[] @relation("saleDetailProduct")

  @@unique([code, name])
}

model SaleDetail {
  id          Int      @id @default(autoincrement())
  quantity    Int
  subtotal    Float
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt
  // Relations
  createdBy   User?    @relation("createdBySaleDetail", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?    @relation("updatedBySaleDetail", fields: [updatedById], references: [id])
  updatedById Int?
  product     Product  @relation("saleDetailProduct", fields: [productId], references: [id])
  productId   Int
  sale        Sale     @relation("saleDetailSale", fields: [saleId], references: [id])
  saleId      Int
}

model Category {
  id          Int      @id @default(autoincrement())
  name        String
  description String
  enabled     Boolean  @default(true)
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt
  // Relations
  createdBy   User?    @relation("createdByCategory", fields: [createdById], references: [id])
  createdById Int?
  updatedBy   User?    @relation("updatedByCategory", fields: [updatedById], references: [id])
  updatedById Int?

  product Product[] @relation("category")

  @@unique([name])
}
