const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const bcrypt = require('bcrypt');
const dotenv = require('dotenv-safe');

dotenv.config({
  example: './.env.template',
});

bcrypt.hash('admin', process.env.PASSWORD_SALT)
.then((password) => prisma.user.create({
  data: {
    password,
    firstName: 'admin',
    lastName: 'admin',
    username: 'admin@disruptivo.io',
  },
}))
.then(() => prisma.scope.create({
  data: {
    name: 'admin',
    rules: [{
      action: ['create', 'read', 'update'],
      subject: ['User', 'Auth', 'Scope', 'Category', 'Customer', 'Invoice', 'Product', 'Sale', 'SaleDetail'],
    }],
    createdById: 1,
  },
}))
.then(() => prisma.user.update({
  data: {
    scopeId: 1,
    createdById: 1,
  },
  where: {
    id: 1,
  },
}))
.then(() => console.log('seed process end'))
.catch((error) => console.log(error.message));